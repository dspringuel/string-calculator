package string_calculator

import (
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func Add_Part1(numbers string) (int, error) {

	if numbers == "" {
		return 0, nil
	}
	numberSlice := strings.Split(numbers, ",")
	sum := 0
	for _, numberString := range numberSlice {
		number, err := strconv.Atoi(numberString)
		if err != nil {
			return 0, err
		}
		sum += number
	}
	return sum, nil
}

func Add_Part2(numbers string) (int, error) {
	numbersWithoutNewLine := strings.ReplaceAll(numbers, "\n", "")
	return Add_Part1(numbersWithoutNewLine)
}

func Add_Part3(numbers string) (int, error) {
	if numbers == "" {
		return 0, nil
	}

	delimiterRegex := regexp.MustCompile("^//(.+)\n")

	// The first element in the slice is the left-most match (ie the control code),
	// and the others are the submatches (i.e. the delimiter in our cases)
	// Since there will be only one submatch, we can safely assume that the delimiter is the
	// at the index 1
	delimiter := delimiterRegex.FindStringSubmatch(numbers)[1]

	numbersWithoutControlCode := delimiterRegex.ReplaceAllString(numbers, "")
	numberSlice := strings.Split(numbersWithoutControlCode, delimiter)
	sum := 0
	for _, numberString := range numberSlice {
		number, err := strconv.Atoi(numberString)
		if err != nil {
			return 0, err
		}
		sum += number
	}
	return sum, nil
}

type NegativeNotAllowedError struct {
	negativeNumbers []int
}

func (n NegativeNotAllowedError) Error() string {
	return fmt.Sprintf("Negatives not allowed: %+v", n.negativeNumbers)
}

func Add_Part4(numbers string) (int, error) {
	if numbers == "" {
		return 0, nil
	}

	delimiterRegex := regexp.MustCompile("^//(.+)\n")

	// The first element in the slice is the left-most match (ie the control code),
	// and the others are the submatches (i.e. the delimiter in our cases)
	// Since there will be only one submatch, we can safely assume that the delimiter is the
	// at the index 1
	delimiter := delimiterRegex.FindStringSubmatch(numbers)[1]

	numbersWithoutControlCode := delimiterRegex.ReplaceAllString(numbers, "")
	numberSlice := strings.Split(numbersWithoutControlCode, delimiter)
	sum := 0
	negativeNumbers := []int{}
	for _, numberString := range numberSlice {
		number, err := strconv.Atoi(numberString)
		if err != nil {
			return 0, err
		}
		if number >= 0 {
			sum += number
		} else {
			negativeNumbers = append(negativeNumbers, number)
		}
	}

	if len(negativeNumbers) > 0 {
		return 0, NegativeNotAllowedError{negativeNumbers: negativeNumbers}
	}
	return sum, nil
}

// I assumed that the multiple delimiters are themselves delimited by a comma (",").
func Add_AllBonuses(numbers string) (int, error) {
	if numbers == "" {
		return 0, nil
	}

	delimiterRegex := regexp.MustCompile("^//(.+)\n")

	// The first element in the slice is the left-most match (ie the control code),
	// and the others are the submatches (i.e. the delimiter in our cases)
	// Since there will be only one submatch, we can safely assume that the delimiter is the
	// at the index 1
	delimitersString := delimiterRegex.FindStringSubmatch(numbers)[1]
	delimiters := strings.Split(delimitersString, ",")

	numbersWithoutControlCode := delimiterRegex.ReplaceAllString(numbers, "")

	numberSlice := []string{numbersWithoutControlCode}
	for _, delimiter := range delimiters {
		newNumberSlice := []string{}
		for _, almostSplitNumber := range numberSlice {
			numberStringForThatDelimiter := strings.Split(almostSplitNumber, delimiter)
			newNumberSlice = append(newNumberSlice, numberStringForThatDelimiter...)
		}
		numberSlice = newNumberSlice
	}

	sum := 0
	negativeNumbers := []int{}
	for _, numberString := range numberSlice {
		number, err := strconv.Atoi(numberString)
		if err != nil {
			return 0, err
		}
		if number >= 0 {
			if number < 1000 {
				sum += number
			}
		} else {
			negativeNumbers = append(negativeNumbers, number)
		}
	}

	if len(negativeNumbers) > 0 {
		return 0, NegativeNotAllowedError{negativeNumbers: negativeNumbers}
	}
	return sum, nil
}
