package string_calculator

import (
	"testing"

	. "github.com/onsi/gomega"
)

type Add func(string) (int, error)

func assertAdd(t *testing.T, addFunction Add, given string, expected int) {
	g := NewGomegaWithT(t)
	sum, err := addFunction(given)
	g.Expect(sum).To(Equal(expected))
	g.Expect(err).To(BeNil())
}
func assertAddWithError(t *testing.T, addFunction Add, given string, expectedError error) {
	g := NewGomegaWithT(t)
	_, err := addFunction(given)
	g.Expect(err).To(Equal(expectedError))
}

func Test_Add_Part1(t *testing.T) {
	assertAdd(t, Add_Part1, "", 0)
	assertAdd(t, Add_Part1, "1,2,5", 8)
	assertAdd(t, Add_Part1, "1,2,3,4,5", 15)
	assertAdd(t, Add_Part1, "65,232347,112232", 344644)
}

func Test_Add_Part2(t *testing.T) {
	assertAdd(t, Add_Part2, "\n", 0)
	assertAdd(t, Add_Part2, "1\n,2,3", 6)
	assertAdd(t, Add_Part2, "1,\n2,4", 7)
	assertAdd(t, Add_Part2, "65,232347,112232\n", 344644)
}

func Test_Add_Part3(t *testing.T) {
	assertAdd(t, Add_Part3, "", 0)
	assertAdd(t, Add_Part3, "//;\n1;3;4", 8)
	assertAdd(t, Add_Part3, "//$\n1$2$3", 6)
	assertAdd(t, Add_Part3, "//@\n2@3@8", 13)
}

func Test_Add_Part4(t *testing.T) {
	assertAdd(t, Add_Part4, "//$\n1$2$3", 6)
	assertAddWithError(t, Add_Part4, "//;\n1;-3;4", NegativeNotAllowedError{negativeNumbers: []int{-3}})
	assertAddWithError(t, Add_Part4, "//$\n-11$2$-3", NegativeNotAllowedError{negativeNumbers: []int{-11, -3}})
	assertAddWithError(t, Add_Part4, "//@\n-2@-53@-8", NegativeNotAllowedError{negativeNumbers: []int{-2, -53, -8}})
}

func Test_Add_AllBonuses(t *testing.T) {
	assertAdd(t, Add_AllBonuses, "", 0)
	assertAdd(t, Add_AllBonuses, "//$\n1$2$3", 6)
	assertAdd(t, Add_AllBonuses, "//$\n1$1001", 1)
	assertAddWithError(t, Add_AllBonuses, "//$\n-1$1001", NegativeNotAllowedError{negativeNumbers: []int{-1}})
	assertAdd(t, Add_AllBonuses, "//***\n1***2***3", 6)
	assertAdd(t, Add_AllBonuses, "//$,@\n1$2@3", 6)
	assertAdd(t, Add_AllBonuses, "//$$%,@\n1$$%2@3$$%4", 10)
	assertAddWithError(t, Add_AllBonuses, "//$$%,@\n1$$%2@3$$%-4", NegativeNotAllowedError{negativeNumbers: []int{-4}})
	assertAdd(t, Add_AllBonuses, "//$$%,@\n1$$%2@3$$%4$$%10002", 10)
	assertAdd(t, Add_AllBonuses, "//$$%,@,***\n1$$%2@3$$%4$$%10002***123***1", 134)
}
