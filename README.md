# String Calculator

## How to run the tests

### Clone repository
* `git clone https://bitbucket.org/dspringuel/string-calculator.git`

* `cd string-calculator`


### -> With go runtime installed
* Verify that go 1.12 is installed: `go version`

* `go test`


### -> Without go runtime installed, but with Docker installed and running
Start a container with golang runtime installed:

* ``docker run -it -v  `pwd`:/string-calculator golang:1.12-stretch /bin/bash``

Then, in the container:

* `cd /string-calculator`

* `go test`